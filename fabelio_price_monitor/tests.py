from django.test import TestCase
from django.test import TestCase, Client
from .forms import AddProductForm
from .views import get_data
from .models import Product


class UnitTest(TestCase):

    def test_page_1_exists(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_page_2_exists(self):
        response = self.client.get('/submitted_link/')
        self.assertEqual(response.status_code, 200)

    def test_page_3_exists(self):
        response = self.client.get('/product/')
        self.assertEqual(response.status_code, 200)

    def test_submit_link_view_uses_correct_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'page_1.html')

    def test_submitted_link_view_uses_correct_template(self):
        response = self.client.get('/submitted_link/')
        self.assertTemplateUsed(response, 'page_2.html')

    def test_product_view_uses_correct_template(self):
        response = self.client.get('/product/')
        self.assertTemplateUsed(response, 'page_3.html')

    def test_web_title(self):
        response = self.client.get('/')
        content = response.content.decode('utf8')
        self.assertIn("Price Monitor", content)

    def test_form_exist(self):
        response = self.client.get('/')
        content = response.content.decode('utf8')
        self.assertIn("<form", content)

    def test_header_exist(self):
        response = self.client.get('/')
        content = response.content.decode('utf8')
        self.assertIn("prima.", content)

    def test_page_1_body_content_exist(self):
        response = self.client.get('/')
        content = response.content.decode('utf8')
        self.assertIn("Welcome,", content)

    def test_page_1_submit_button(self):
        response = self.client.get('/')
        content = response.content.decode('utf8')
        self.assertIn("<button", content)

    def test_fill_url_form(self):
        data = {
            'url': 'https://fabelio.com/ip/kursi-cessi-upholstered.html',
        }
        form = AddProductForm(data=data)
        self.assertTrue(form.is_valid())

    def test_page_2_body_text_exist(self):
        response = self.client.get('/submitted_link/')
        content = response.content.decode('utf8')
        self.assertIn("Submitted Link", content)

    def test_page_2_table_exist(self):
        response = self.client.get('/submitted_link/')
        content = response.content.decode('utf8')
        self.assertIn("<table", content)

    def test_page_3_body_content_exist(self):
        response = self.client.get('/product/')
        content = response.content.decode('utf8')
        self.assertIn("No Product to Display", content)

    def test_create_objects_in_models_form(self):
        url = 'https://fabelio.com/ip/kursi-cessi-upholstered.html'
        data = get_data(url)
        Product.objects.create(
            url=url,
            title=data['title'],
            current_price=data['current_price'],
            description=data['description'],
        )
        self.assertEqual(Product.__str__(Product), Product.title)

    def test_views_get_data(self):
        url = 'https://fabelio.com/ip/kursi-cessi-upholstered.html'
        data = get_data(url)
        self.assertEqual(data['title'], 'Kursi Cessi Upholstered')

    def test_get_data(self):
        url = 'https://fabelio.com/ip/kursi-cessi-upholstered.html'
        data = get_data(url)
        Product.objects.create(
            url=url,
            title=data['title'],
            current_price=data['current_price'],
            description=data['description'],
        )
        response = self.client.get(
            '/product/Kursi Cessi Upholstered')
        self.assertNotEqual(response.content, {
            'title': 'Kursi Cessi Upholstered'})
