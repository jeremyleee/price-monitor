from django.urls import path
from . import views

app_name = 'fabelio_price_monitor'

# url's for fabelio price monitor app
urlpatterns = [
    path('', views.price_monitor_view, name='submit_link'),
    path('submitted_link/', views.submitted_link, name='submitted_link'),
    path('product/', views.product, name='product'),
    path('product/<title>', views.product, name='product')

]
