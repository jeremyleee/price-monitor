from django.apps import AppConfig


class FabelioPriceMonitorConfig(AppConfig):
    name = 'fabelio_price_monitor'
