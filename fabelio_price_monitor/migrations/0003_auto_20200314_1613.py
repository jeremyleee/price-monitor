# Generated by Django 3.0.4 on 2020-03-14 09:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fabelio_price_monitor', '0002_auto_20200314_1555'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='description',
            field=models.CharField(max_length=2000),
        ),
    ]
