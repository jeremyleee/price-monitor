import time
from celery import shared_task
from .models import Product
from fabelio_price_monitor.views import get_data


@shared_task
def latest_price():
    items = Product.objects.all()
    for item in items:
        data = get_data(item.url)
        if data['current_price'] != item['current_price']:
            item['current_price'] = data['current_price']
            item.save()


while True:
    latest_price()
    time.sleep(3600)
