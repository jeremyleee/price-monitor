from django.db import models


# Django models for fabelio price monitor
class Product(models.Model):
    title = models.TextField(max_length=200)
    image = models.ImageField(width_field=100, height_field=100, null=True)
    url = models.TextField(max_length=600)
    current_price = models.TextField(max_length=200)
    description = models.TextField(max_length=2000)

    def __str__(self):
        return self.title
