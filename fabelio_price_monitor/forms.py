from django import forms
from .models import Product

# Django form for page 1 with text input box to get the product page link


class AddProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['url']
        widgets = {
            'url': forms.TextInput(attrs={'class': 'input', 'placeholder': 'Submit your link ...'})
        }
