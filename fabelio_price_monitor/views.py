from django.shortcuts import render, get_object_or_404, HttpResponseRedirect
from urllib.parse import quote
from urllib.request import urlopen, Request
from bs4 import BeautifulSoup
from .models import Product
from .forms import AddProductForm


# view for page 1
# @param request
# @return render product page or submitted link page if url already existed
def price_monitor_view(request):
    products = Product.objects.all()
    form = AddProductForm(request.POST)
    try:
        if request.method == 'POST':
            if form.is_valid():
                # get the url from the form
                url = form.cleaned_data.get('url')
                for product in products:
                    # redirect to page 2 (submitted link page) if the link already submitted before
                    if url == product.url:
                        return HttpResponseRedirect('submitted_link')
                data = get_data(url)
                Product.objects.create(
                    url=url,
                    title=data['title'],
                    current_price=data['current_price'],
                    description=data['description'],
                )
                # redirect to page 3 (product page) after submitting the product page link
                return HttpResponseRedirect('product/'+data['title'])
    except ValueError:
        return HttpResponseRedirect('/')
    except IndexError:

        return HttpResponseRedirect('/')
    else:
        form = AddProductForm()
    context = {
        'form': form,
    }

    submit_link = "page_1.html"
    return render(request, submit_link, context)


# view for page 2
# @param request
# @return render submitted link page
def submitted_link(request):
    products = Product.objects.order_by('-id')
    context = {
        'products': products,
    }
    return render(request, 'page_2.html', context)

# view for page 3, get title parameter from url
# @param request,title
# @return render product page


def product(request, title=None):
    product = None
    products = Product.objects.all()
    for product in products:
        # check if title exist in database
        if title:
            # return product with the same title with the parameter
            product = Product.objects.get(title=title)

        else:
            # if doesnt exist return none
            product = None
    context = {
        'product': product,
    }
    return render(request, 'page_3.html', context)


# get data from url, parsing fabelio product page
# @param url
# @return product object for Django models
def get_data(url):
    req = Request(url, headers={
                  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.132 Safari/537.36'})
    html = urlopen(req).read()
    # initialize beautifulsoup
    soup = BeautifulSoup(html, 'html.parser')
    # find product title
    title = soup.find_all(
        'h1', {"class": "page-title"})[0].find('span').get_text()
    # find product current price
    current_price = soup.find_all(
        'span', {"class": "price"})[0].get_text()
    # find product description
    description = soup.find('div', id="description").get_text()

    # image = soup.find_all('div', {"class": "product media"})
    # print(image)
    return {'title': title, 'current_price': current_price, 'description': description}
