import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Price_Monitor.settings')

app = Celery('fabelio_price_monitor')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
