## Price Monitor Web App

The web app is called prima, which stands for price monitor app <br>
It was written in Python (Django Framework), HTML and CSS <br>
It consists of 3 pages <br>

## API Documentation:

1. Page 1 ( fabelio-prima.herokuapp.com/) <br>
   1. Page that allows you to submit fabelio’s product page link ( Submit Link page)
   2. If you submit an existing fabelio’s product page link, it will redirect you to page 2 ( Submitted Link page)
   3. Please submit product page link to the form ( ex: https://fabelio.com/ip/fabelio-cloud-latex-mattress.html)
      <br>
      <br>
2. Page 2 ( fabelio-prima.herokuapp.com/submitted_link/)
   <br>
   1. Page that shows submitted fabelio’s product page link ( Submitted Link page)
   2. It will show you submitted links, product name, current price and a link to Product page (page 3) in a table
      <br>
      <br>
3. Page 3 ( fabelio-prima.herokuapp.com/product/:product.title)
   <br>
   1. Page that displays submitted fabelio’s product
   2. It will show you product description, image, title, and price.
      <br>

## Deploy Instruction

1. Sign in or Sign up to Herokuapp.com <br>
2. Create new app <br>
3. Open Gitlab Repository > Settings > CI/CD > Variables. Add 3 Variables:
   - Key-Variable :
     - HEROKU_APIKEY ( you can find it at heroku.com account settings)
     - HEROKU_APPNAME - APP-NAME
     - HEROKU_APP_HOST - APP-NAME.herokuapp.com<br><br>
4. Add `whitenoise.middleware.WhiteNoiseMiddleware` in project/settings.py inside the `MIDDLEWARE`. <br><br>

```python
MIDDLEWARE = [
'django.middleware.security.SecurityMiddleware',
'django.contrib.sessions.middleware.SessionMiddleware',
'django.middleware.common.CommonMiddleware',
'django.middleware.csrf.CsrfViewMiddleware',
'django.contrib.auth.middleware.AuthenticationMiddleware',
'django.contrib.messages.middleware.MessageMiddleware',
'django.middleware.clickjacking.XFrameOptionsMiddleware',
'whitenoise.middleware.WhiteNoiseMiddleware',]
```

<br>

5. Add `STATIC_ROOT` in project/settings.py <br><br>

```python
   STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
```

   <br>

6. Add .gitignore file <br>

7. Add gitlab-ci.yml file <br>
8. Push your project to gitlab <br>
